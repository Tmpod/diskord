package com.jessecorbett.diskord.internal

internal const val defaultUserAgentUrl = "https://gitlab.com/jesselcorbett/Diskord"
internal const val defaultUserAgentVersion = "1.3.2"
