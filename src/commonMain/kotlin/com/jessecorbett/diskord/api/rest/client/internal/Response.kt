package com.jessecorbett.diskord.api.rest.client.internal

data class Response(val code: Int, val body: String?, val headers: Map<String, String?>)
